<?php
/**
 * OpenArdilla v2
 * La logique provient d'OpenArdilla.
 * Sauf qu'au lieu d'essayer de maintenir MagPieRSS et Snoopy ad-nauseam
 * j'utilise désormais une lib plus récente : SimplePie.
 * Pas forcément la meilleur approche (il me faudrait un truc tout simple),
 * mais j'ai pas envie de me prendre la tête avec l'écriture d'un parseur xml.
 * https://gitlab.com/chibinah/openardilla
 * Licence GPL v3
 */

    require 'config.php';
    require 'vendor/SimplePie.compiled.php';

    if( isset($_GET['url']) )
        ajaxParser($feeds);
    else
        die();

    /**
     * On récupère l'id du flux (son n° dans l'array).
     *
     */
    function ajaxParser($feeds)
    {
        $urlID = $_GET['url'];

        if($urlID >= 0 && $urlID < count($feeds))
        {
            $url = $feeds[$urlID];

            $singleFeed = parseRSS($url);
            $singleFeed = str_replace("&_=", "", $singleFeed);
            echo $singleFeed;
        }
        else
        {
            echo '<div class="error">Erreur !</div>';
        }
    }


    /**
     * Lecture du flux.
     *
     */
    function parseRSS($url)
    {
        //nombre de titres à afficher
        $numHeadlines = 12;

        $pie = new SimplePie();
        $pie -> set_feed_url($url);
        $pie -> enable_cache(TRUE);
        $pie -> init();
        $pie -> handle_content_type();

        $items = $pie -> get_items();

        if($pie -> error())
        {
            return
                  '<div class="error">Erreur !<br />'
                . '<a href="' . $url . '">' . $url . '</a>'
                . '<p>' . htmlspecialchars($pie -> error()) . '</p>'
                .'</div>'
            ;
            exit;
        }

        $feed = '';

        $feed .= '<h1><a href="' . $pie -> get_link() . '" target="_blank">' . $pie -> get_title().'</a></h1>';
        $feed .= '<ul class="feed">';

        $increment = 1;
        foreach ($pie -> get_items() as $item) {

            $href = strip_tags($item -> get_link());
            $title = strip_tags($item -> get_title());
            $titleLong = strip_tags($item -> get_title());

            if(strlen($title) > 43){ $title = substr_replace($title,"...",43); }

            $feed .= '
                        <li>
                            <a href="'.$href.'" target="_blank" title="'.$titleLong.'" >- '.$title.'</a>
                        </li>
                        ';
            $increment++;
            //si on a atteint le nombre de titres à afficher, on sort
            if($increment > $numHeadlines)
                break;
        }

        $feed .= "</ul>";
        return $feed;
    }
?>
