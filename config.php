<?php

/**
 * OpenArdilla v2
 * Dans l'acienne version de OpenArdilla (0.1), les flux étaient stockés dans une
 * base de données mysql ou mariadb.
 * Comme ça sert en fait à rien, autant mettre cette liste
 * directement dans un array PHP. ça simplifie énormément
 * le code, et un simple hébergement php suffit.
 *
 * https://gitlab.com/chibinah/openardilla
 * Licence GPL v3
 *
 * Liste des flux rss.
 * insérer l'URL complète, séparées par une virgule. Cf. exemple ci dessous
 */
$feeds = array
(
    "https://blog.chibi-nah.fr/feeds/all.atom.xml",
    "http://example.com/rss"
);

?>
