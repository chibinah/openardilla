<?php

/**
 * OpenArdilla v2
 * Réécriture de OpenArdilla, en utilisant des trucs un peu plus moderne
 * comme html5 et php8.2
 * https://gitlab.com/chibinah/openardilla
 * Licence GPL v3
 */
    require 'config.php';
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Open Ardilla v2</title>
    <link href="css/ardilla.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="js/prototype.js"></script>
    <script type="text/javascript" src="js/ajaxRSS.js"></script>
</head>
<body>
    <div id="container">
        <div id="menu">
            <ul>
                <li>Flux Rss</li>
                <li><a href="https://gitlab.com/chibinah/openardilla">Code source</a></li>
            </ul>
        </div>
<?php

    //On insère autant de cellules que de flux rss.
    //code repris de OpenArdilla
    echo '<div id="contenu">';

    $feedindex = 0;
    foreach($feeds as $url=>$numHeadlines)
    {
        $feedindex++;
    ?>

    <div class="feedbox">
    <div id="feed<?=$feedindex?>" class="feed loadingfeed" data-feed="<?=$url?>" title="<?=$numHeadlines?>"></div>
    </div>

    <?php
    }

    echo '</div>';
?>

    </div>
</body>
</html>
