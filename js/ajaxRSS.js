/* Vince a.k.a groGeek 2010*/
Event.observe(window, 'load', initRSS, false);

function initRSS(){

    if (document.getElementsByTagName) {

        let feedList = document.getElementsByClassName("feed");

        for (let feed of feedList) {

            let feedID = feed.dataset.feed;
            getFeed(feedID,feed.getAttribute("id"));
        }
    }
}

function getFeed(feedID,boxId) {

    $(boxId).innerHTML = '<img src="images/loading.gif" alt="Loading RSS Headlines" class="loading" />';

    // Prepare GET variables needed for PHP to parse RSS
    let pars = 'url=' + feedID;

    // Go-go gadget Ajax call using Prototype
    // Auto-update feeds every 10 minute (600 seconds)
    let MyAjaxUpdater = new Ajax.PeriodicalUpdater(boxId, 'parseRSS.php', {method:'get', parameters:pars, asynchronous:true, frequency:600});
}
