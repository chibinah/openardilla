/* Vince a.k.a groGeek 2010*/
function myPreviewOn(theLink) {
    var newHTML = "<iframe id='framePreview'  src='"+ theLink +"' height='600px' width='800px'></iframe>";
    document.getElementById('someid').innerHTML = newHTML;
}

function myPreviewOff() {
    var maFrame =  document.getElementById('framePreview');
    document.getElementById('someid').removeChild(maFrame);

}

document.onmousemove = function(e) {

    var mX = parseInt(e.pageX ? e.pageX : e.clientX, 10);
    var mY = parseInt(e.pageY ? e.pageY : e.clientY, 10);

    if (document.all) {
        mX += document.documentElement.scrollLeft;
        mY += document.documentElement.scrollTop;
    }

    var width = document.getElementById('flux').offsetWidth;
    mX = Math.min(Math.max(0, width), 150);

    document.getElementById("someid").style.left = mX + 'px';
    document.getElementById("someid").style.top = mY + 'px';
}
