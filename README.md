# OpenArdilla

![Logo d'OpenArdilla, représentant un écureil avec un logo RSS au niveau de sa queue](ardilla.png)

OpenArdilla v2 est une réécriture/fork de OpenArdilla (beta 0.1).

https://web.archive.org/web/20111127143952/http://openardilla.org/?page_id=13

## Description

« Ardilla » est la traduction espagnole de « Écureuil ». Ce nom a été choisi en référence au comportement du rongeur qui engrange sa nourriture de la même façon que Ardilla engrange vos flux RSS, marques-pages et notes. 

L’installation de Ardilla se fait sur un serveur web, il est donc accessible directement depuis votre navigateur. 

Ardilla repose sur les technologies web PHP, (MySQL n'est plus utilisé et a été retiré), Javascript + Ajax, CSS.

C’est un projet libre distribué selon les conditions de la licence GPL 3.0.

Dans cette version 2, seul l'aggrégateur de flux RSS/Atom a été conservé. la gestion des marques-pages et des notes n'est plus présente dans cette version.

Les dépendances vers MagpieRSS et Snoopy ont été retirées. Le traitement des flux RSS/Atom est maitenant géré par SimplePie.

OpenArdilla v2 est mono-utilisateur/mono-utilisatrice, en consultation directe, sans gestion de compte.

## Visuels

OpenArdilla s'adapte automatiquement en proposant un thème clair ou sombre, en utilisant MediaQueries.

![Capture d'écran avec un thème clair](screenshots/OpenArdilla.png)

![Capture d'écran avec un thème sombre](screenshots/OpenArdilla-dark.png)

## Prérequis

* PHP 7.2 ou supérieur (testé avec succès avec PHP 8.2)
* libxml2
* iconv ou nbstring ou intl
* cURL
* support des PCRE

## Installation

L'installation s'effectue chez n'importe quel hébergeur proposant une version de PHP récente. Aucune base de données n'est nécessaire.

L'installation consiste à :

* déposer les fichiers dans un répertoire accessible sur le web ;
* modifier le fichier config.php et renseigner à l'intérieur la liste des URL vers les flux RSS/Atom ;
* permettre l'écriture dans le répertoire cache par le serveur web (mise en cache des flux RSS/Atom).

C'est tout.

## Auteurs du projet original

* Code : GroGeek
* Test et publication : TimCruz
* Logo : yeKcim

## License

OpenArdilla est publié sous licence GNU General Public License version 3.0 (GPLv3).
SimplePie, librairie pour le traitement des flux RSS/Atom est sous licence BSD-3.

https://github.com/simplepie/simplepie

## État du projet

Le projet étant stable, seule une maintenance légère est prévue à terme pour suivre les évolutions de PHP et de SimplePie.
